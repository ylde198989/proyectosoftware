import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ServiceProvider Provider');
  }

  activades: any;

  readonly url: string = 'https://software2-2018.herokuapp.com/api/user/login';
  readonly url2: string = 'https://software2-2018.herokuapp.com/api/users';
  readonly url3: string = 'https://software2-2018.herokuapp.com/api/activities';

  login(body) {

    return this.http.post(this.url, body);
  }

  getActivitiesUser(token) {
    const headers = new HttpHeaders()
      .set("Authorization", 'Bearer' + token)
      .set("Accept", "application/json");

    return this.http.get(this.url3, { headers });
  }


  getUsers(token) {
    const headers = new HttpHeaders()
      .set("Authorization", 'Bearer' + token)
      .set("Accept", "application/json");

    return this.http.get(this.url2, { headers });
  }

}
