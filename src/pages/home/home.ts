import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { ActividadDetallesPage } from '../actividad-detalles/actividad-detalles';
import { UsuariosDetallesPage } from '../usuarios-detalles/usuarios-detalles';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  myInput: any;
  items: any;
  prueba: any;
  prueba2: any;
  buscador: string;
  token: string = '';
  users: any;
  spinner: Boolean;
  usuario: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams, private service: ServiceProvider) {
    this.token = this.navParams.get('token');
    this.getActividad();
    this.getUsers();
    this.spinner = true;
  }

  getActividad() {
    let token = localStorage.getItem('token');
    this.service.getActivitiesUser(token).subscribe((data: any) => {
      this.spinner = false;
      this.items = data.data;
    })
  }



  getUsers() {
    let token = localStorage.getItem('token');
    this.service.getUsers(token).subscribe((data: any) => {
      this.spinner = false;
      console.log(data.data);
      this.users = data.data;
    })
  }

  onInput(ev: any) {
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.users = this.users.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1)
      })
    } else {
      this.getUsers();
    }
  }

  onInput2(ev: any) {
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.description.toLowerCase().indexOf(val.toLowerCase()) > -1)
      })
    } else {
      this.getActividad();
    }
  }

  irActividad2(actividad2) {
    console.log(actividad2);

    let actividad = actividad2;

    // let actividades = {
    //   capacity: Number,
    //   description: String,
    //   end_date: Date,
    //   start_date: Date,
    //   type: String,
    // }

    // let evaluations = {
    //   capacity: Number,
    //   description: String,
    //   end_date: Date,
    //   start_date: Date,
    //   type: String
    // }

    // let personalInfo = {
    //   address: actividad2.address,
    //   email: actividad2.email,
    //   name: actividad2.name,
    //   phone: actividad2.phone
    // }

    // let presentation = {
    //   capacity: Number,
    //   description: String,
    //   end_date: Date,
    //   start_date: Date,
    //   type: String
    // }

    // actividad2.activities.forEach(element => {
    //   actividades.capacity = element.capacity,
    //     actividades.description = element.description,
    //     actividades.end_date = element.end_date,
    //     actividades.start_date = element.start_date,
    //     actividades.type = element.type;
    // });

    // actividad2.evaluations.forEach(element => {
    //   evaluations.capacity = element.capacity,
    //     evaluations.description = element.description,
    //     evaluations.end_date = element.end_date,
    //     evaluations.start_date = element.start_date,
    //     evaluations.type = element.type;
    // });

    // actividad2.presentations.forEach(element => {
    //   presentation.capacity = element.capacity,
    //     presentation.description = element.description,
    //     presentation.end_date = element.end_date,
    //     presentation.start_date = element.start_date,
    //     presentation.type = element.type;
    // });
    console.log(actividad);
    this.navCtrl.push(UsuariosDetallesPage, {
      actividad: actividad
      // infor:personalInfo,
      // activities: actividades,
      // evaluations: evaluations,
      // presentations: presentation
    });
  }

  irActividad(actividad) {
    let evaluations = {
      address: String,
      created_at: Date,
      email: String,
      name: String
    };

    let event = {
      description: actividad.event.description,
      end_date: actividad.event.end_date,
      start_date: actividad.event.start_date
    };

    let necessities = {
      description: String
    };

    let places = {
      capacity: actividad.place.capacity,
      description: actividad.place.description,
      location: actividad.place.location
    };

    let referee = {
      address: String,
      email: String,
      name: String,
      phone: String
    };

    actividad.evaluations.forEach(element => {
      evaluations.address = element.address,
        evaluations.created_at = element.created_at,
        evaluations.email = element.email,
        evaluations.name = element.name
    });

    actividad.necessities.forEach(element => {
      necessities.description = element.description
    });

    actividad.referees.forEach(element => {
      referee.address = element.address,
        referee.email = element.email,
        referee.name = element.name,
        referee.phone = element.phone
    });

    this.navCtrl.push(ActividadDetallesPage, {
      id: actividad.id,
      capacity: actividad.capacity,
      descripcionActividad: actividad.description,
      creacionActividad: actividad.created_at,
      inicioActividad: actividad.start_date,
      finActividad: actividad.end_date,
      tipoActividad: actividad.type,
      evaluation: evaluations,
      event: event,
      necessitie: necessities,
      place: places,
      referis: [{
        referi: referee
      }],
      responsable: {
        email: actividad.responsible.email,
        name: actividad.responsible.name,
        phone: actividad.responsible.phone
      },
      tematica: actividad.thematic.description,
      token: localStorage.getItem('token')
    });
  }


}
