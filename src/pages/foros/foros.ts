import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { ServiceProvider } from '../../providers/service/service';
import { ActividadDetallesPage } from '../actividad-detalles/actividad-detalles';

/**
 * Generated class for the ForosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foros',
  templateUrl: 'foros.html',
})
export class ForosPage {

  actividades: any;
  spinner: Boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private serviceProvider: ServiceProvider,
    public http: HttpClient) {
    this.getActividades();
    this.spinner = true;
  }


  getActividades() {
    let token = localStorage.getItem('token');
    this.serviceProvider.getActivitiesUser(token)
      .subscribe((data: any) => {
        this.spinner = false;
        console.log(data.data);
        this.actividades = data.data;
      })
  }

  irActividad(actividad) {
    let evaluations = {
      address: String,
      created_at: Date,
      email: String,
      name: String
    };

    let event = {
      description: actividad.event.description,
      end_date: actividad.event.end_date,
      start_date: actividad.event.start_date
    };

    let necessities = {
      description: String
    };

    let places = {
      capacity: actividad.place.capacity,
      description: actividad.place.description,
      location: actividad.place.location
    };

    let referee = {
      address: String,
      email: String,
      name: String,
      phone: String
    };

    actividad.evaluations.forEach(element => {
      evaluations.address = element.address,
        evaluations.created_at = element.created_at,
        evaluations.email = element.email,
        evaluations.name = element.name
    });

    actividad.necessities.forEach(element => {
      necessities.description = element.description
    });

    actividad.referees.forEach(element => {
      referee.address = element.address,
        referee.email = element.email,
        referee.name = element.name,
        referee.phone = element.phone
    });

    this.navCtrl.push(ActividadDetallesPage, {
      id: actividad.id,
      capacity: actividad.capacity,
      descripcionActividad: actividad.description,
      creacionActividad: actividad.created_at,
      inicioActividad: actividad.start_date,
      finActividad: actividad.end_date,
      tipoActividad: actividad.type,
      evaluation: evaluations,
      event: event,
      necessitie: necessities,
      place: places,
      referis: [{
        referi: referee
      }],
      responsable: {
        email: actividad.responsible.email,
        name: actividad.responsible.name,
        phone: actividad.responsible.phone
      },
      tematica: actividad.thematic.description,
      token: localStorage.getItem('token')
    });
  }

}
