import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForosPage } from './foros';

@NgModule({
  declarations: [
    ForosPage,
  ],
  imports: [
    IonicPageModule.forChild(ForosPage),
  ],
})
export class ForosPageModule {}
