import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PonenciasPage } from './ponencias';

@NgModule({
  declarations: [
    PonenciasPage,
  ],
  imports: [
    IonicPageModule.forChild(PonenciasPage),
  ],
})
export class PonenciasPageModule {}
