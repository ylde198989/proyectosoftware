import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

//servicio
import { ServiceProvider } from '../../providers/service/service'

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  token: any;
  login: FormGroup;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private serviceProvider: ServiceProvider,
    private loadingCtrl: LoadingController) {



    this.login = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Bienvenido....'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 2000);
  }

  logForm() {
    this.serviceProvider.login(this.login.value)
      .subscribe((data: any) => {
        this.token = localStorage.setItem('token', data.data)
        this.presentLoadingDefault();
        this.navCtrl.setRoot(HomePage);
      });
  }

}
