import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActividadDetallesPage } from './actividad-detalles';

@NgModule({
  declarations: [
    ActividadDetallesPage,
  ],
  imports: [
    IonicPageModule.forChild(ActividadDetallesPage),
  ],
})
export class ActividadDetallesPageModule {}
