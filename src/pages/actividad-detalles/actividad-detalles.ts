import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ActividadDetallesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-actividad-detalles',
  templateUrl: 'actividad-detalles.html',
})
export class ActividadDetallesPage {

  descripcionActividad: string;
  inicioActividad: Date;
  finActividad: Date;
  tipoActividad: string;
  capacity: number;
  tematica: String;
  responsable = {
    email: String,
    name: String,
    phone: String
  };
  referis = [{
    referi: [{
      email: String,
      name: String,
      phone: String
    }]
  }];
  places = {
    capacity: Number,
    description: String,
    location: String
  };

  token: string;
  items: any[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.capacity = this.navParams.get('capacity');
    this.descripcionActividad = this.navParams.get('descripcionActividad');
    this.finActividad = this.navParams.get('finActividad');
    this.tipoActividad = this.navParams.get('tipoActividad');
    this.inicioActividad = this.navParams.get('inicioActividad');
    this.tematica = this.navParams.get('tematica');
    this.responsable = this.navParams.get('responsable');
    this.referis = this.navParams.get('referis');
    this.places = this.navParams.get('place');
  }


}
