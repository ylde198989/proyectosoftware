import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ConferenciasPage } from '../pages/conferencias/conferencias';
import { LoginPage } from '../pages/login/login';
import { PonenciasPage } from '../pages/ponencias/ponencias';
import { ForosPage } from '../pages/foros/foros';
import { HomePage } from '../pages/home/home';
//import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  //rootPage: any = ConferenciasPage;
  rootPage: any = LoginPage;

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen) {
    console.log(localStorage.getItem('token'));
    if (localStorage.getItem('token') != null) {
      this.rootPage = HomePage;
    } else {
      this.rootPage = LoginPage;
    }
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Conferencias', component: ConferenciasPage },
      { title: 'Presentaciones', component: PonenciasPage },
      { title: 'Foros', component: ForosPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  salir() {
    localStorage.clear();
    this.nav.setRoot(LoginPage)
  }
}
