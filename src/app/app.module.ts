import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { ConferenciasPage } from '../pages/conferencias/conferencias';
import { LoginPage } from '../pages/login/login';
import { PonenciasPage } from '../pages/ponencias/ponencias';
import { ActividadDetallesPage } from '../pages/actividad-detalles/actividad-detalles';
import { ForosPage } from '../pages/foros/foros';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServiceProvider } from '../providers/service/service';
import { HomePage } from '../pages/home/home';
import { UsuariosDetallesPage } from '../pages/usuarios-detalles/usuarios-detalles';



@NgModule({
  declarations: [
    MyApp,
    ConferenciasPage,
    LoginPage,
    ActividadDetallesPage,
    PonenciasPage,
    ForosPage,
    HomePage,
    UsuariosDetallesPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ReactiveFormsModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ConferenciasPage,
    LoginPage,
    ActividadDetallesPage,
    PonenciasPage,
    ForosPage,
    HomePage,
    UsuariosDetallesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ServiceProvider
  ]
})
export class AppModule { }
